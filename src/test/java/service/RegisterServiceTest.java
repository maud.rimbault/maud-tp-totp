package service;

import data.UserRepository;
import generator.RandomKeyGenerator;
import org.junit.jupiter.api.*;
import org.mockito.Mockito;
import user.User;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RegisterServiceTest {


    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
    }

 //   @Test
//    void TestRegisterService(){
//        String email = "test@test.fr";
//        RegisterService registerService = new RegisterService(new RandomKeyGenerator(new Random()), new UserRepository());
//        registerService.register(email);
//        assertEquals(1, UserRepository.getUsersList().size());
//    }

    @Test
    void nominalCase() {
        // infos Mockito : https://javadoc.io/doc/org.mockito/mockito-core/latest/org/mockito/Mockito.html#stubbing
        // GIVEN
        RandomKeyGenerator secretKeyGeneratorMock = Mockito.mock(RandomKeyGenerator.class);
        UserRepository userRepositoryMock = Mockito.mock(UserRepository.class);

        Mockito.when(secretKeyGeneratorMock.generateKey()).thenReturn(12345678L);

        RegisterService registerService = new RegisterService(secretKeyGeneratorMock, userRepositoryMock);

        // WHEN
        final Long secretKey = registerService.register("ba@zenika.com");

        // THEN
        assertEquals(12345678L, secretKey);
        Mockito.verify(userRepositoryMock).save(Mockito.eq(new User("ba@zenika.com", 12345678L)));
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }
}
