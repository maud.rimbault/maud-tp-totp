package generator;

import data.UserRepository;
import org.junit.jupiter.api.*;
import service.RegisterService;

import java.util.Random;

import static org.junit.jupiter.api.Assertions.*;

public class RandomKeyGeneratorTest {

    RegisterService registerService;
    RandomKeyGenerator randomKeyGenerator;
    UserRepository userRepository;

    @BeforeAll
    static void initAll() {
        System.out.println("Lancement des tests");
    }

    @BeforeEach
    void init() {
        System.out.println("Lancement d'un test");
        registerService = new RegisterService(randomKeyGenerator, userRepository);
    }

    @Test
    void TestRandomKeyGenerator(){
        RandomKeyGenerator randKey = new RandomKeyGenerator(new Random(4548L));
        assertEquals(2327990690379593836L, randKey.generateKey());
    }

    @AfterEach
    void tearDown() {
        System.out.println("Fin du test");
    }

    @AfterAll
    static void tearDownAll() {
        System.out.println("Fin des tests");
    }
}
