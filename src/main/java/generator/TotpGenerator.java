package generator;

import java.sql.Timestamp;
import java.time.Instant;
import java.time.LocalDateTime;

public class TotpGenerator {
    public Long generate(Long secret) {
        //Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        //Long currentTimestampSec = timestamp.getTime() / 30000;
        Long currentTimestampSec = Instant.now().getEpochSecond() / 300;
        Long sumKeyTime = currentTimestampSec + secret;
        return sumKeyTime % 1000000;
    }
}
