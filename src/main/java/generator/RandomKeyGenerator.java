package generator;

import java.util.Random;

public class RandomKeyGenerator {
    private Random randomKey;

    public RandomKeyGenerator(Random random) {
        this.randomKey = random;
    }


    public Long generateKey(){
        return Math.abs(this.randomKey.nextLong());
        //return String.valueOf(Math.abs(this.randomKey.nextLong()));
    }
}
