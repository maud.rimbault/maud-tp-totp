import data.UserRepository;
import generator.RandomKeyGenerator;
import generator.TotpGenerator;
import service.LoginService;
import service.RegisterService;

import java.sql.Timestamp;
import java.time.Instant;
import java.util.Random;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
        UserRepository userRepository = new UserRepository();
        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
        TotpGenerator totpGenerator = new TotpGenerator();
        LoginService loginService = new LoginService(userRepository);


        while (true) {

            final String[] action;
            System.out.println("Tapez l'action à effectuer");
            final String input = new Scanner(System.in).nextLine();
            action = input.split(" ");
            if (action.length == 0) continue;

            switch (action[0]) {
                //taper register mr@test.fr
                case "register":
                    Long newUserKey = registerService.register(action[1]);
                    System.out.println("Secret key : " + newUserKey + ". Store it in a safe place.");
                    break;
                //taper generate 5410309788464053313
                case "generate":
                    Long code = totpGenerator.generate(Long.parseLong(action[1]));
                    System.out.println("Le code actuel est " + code);
                    break;
                //taper login mr@test.fr
                case "login":
                    final boolean loginSuccess = loginService.attemptLogin(action[1], Long.parseLong(action[2]));
                    System.out.println(loginSuccess ? "Ok, you're in !" : "Sorry, that's not right...");
                    break;
                case "stop":
                    System.exit(0);
                default:
                    System.out.println("Erreur : action inconnue");
                    break;
            }
        }
    }
}


//    public static void main(String[] args) {
//        System.out.println("Veuillez indiquer votre email : ");
//        Scanner scanner = new Scanner(System.in);
//        String email = scanner.next();
//
//        RandomKeyGenerator randomKeyGenerator = new RandomKeyGenerator(new Random());
//        UserRepository userRepository = new UserRepository();
//        RegisterService registerService = new RegisterService(randomKeyGenerator, userRepository);
//        System.out.println(registerService.register(email));
//
//        TotpGenerator totpGenerator = new TotpGenerator();
//        System.out.println(totpGenerator.generate(registerService.register(email)));
//
//    }