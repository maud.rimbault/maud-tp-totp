package data;

import user.User;

import java.util.HashMap;

public class UserRepository {

    private HashMap<String, User> usersList = new HashMap<>();

    public void save(User newUser) {
        usersList.put(newUser.getEmail(), newUser);
    }

    public HashMap<String, User> getUsersList() {
        return this.usersList;
    }

    public User findUserByEmail(String email) {
        return this.usersList.get(email);
    }


    //    @Override
//    public String toString() {
//        return super.toString();
//    }
}
