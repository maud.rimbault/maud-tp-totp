package user;

import java.util.Objects;

public class User {
    private String email;
    private Long randomKey;

    public User(String email, Long randomKey){
        this.email = email;
        this.randomKey = randomKey;
    }

    public String getEmail() {
        return email;
    }

    public Long getRandomKey() {
        return randomKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(email, user.email) && Objects.equals(randomKey, user.randomKey);
    }

//    Solution pour teste si 2 user idem (regarde si 2 instances idem or ici non donc regarde propriété ici email) Benoit
//    @Override
//    public boolean equals(Object obj) {
//        if(! (obj instanceof User)){
//            return false;
//        } else {
//            User other = (User) obj;
//            return other.email.equals(this.email) && other.randomKey.equals(this.randomKey);
//        }
//    }
}
