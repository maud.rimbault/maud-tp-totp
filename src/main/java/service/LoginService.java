package service;

import data.UserRepository;
import generator.TotpGenerator;
import user.User;

import java.util.Objects;

public class LoginService {

    private UserRepository userRepository;

    public LoginService(UserRepository userRepository){
        this.userRepository = userRepository;
    }

    public boolean attemptLogin(String email, Long code){
        TotpGenerator totpGenerator = new TotpGenerator();
        User currentUser = userRepository.findUserByEmail(email);
        Long loginCode = totpGenerator.generate(currentUser.getRandomKey());

        return Objects.equals(code, loginCode);

//        if (Objects.equals(code, loginCode)){
//            return true;
//        } else {
//            return false;
//        }
    }

}
