package service;

import data.UserRepository;
import generator.RandomKeyGenerator;
import user.User;

import java.util.Random;

public class RegisterService {

    private final RandomKeyGenerator randomKeyGenerator;
    private final UserRepository userRepository;

    public RegisterService(RandomKeyGenerator randomKeyGenerator, UserRepository userRepository){
        this.randomKeyGenerator = randomKeyGenerator;
        this.userRepository = userRepository;
    }

    public Long register(String email) {
        final Long newKey = this.randomKeyGenerator.generateKey();
        //Long newKey = new RandomKeyGenerator(new Random()).generateKey();
        User newUser = new User(email, newKey);
        userRepository.save(newUser);
        return newUser.getRandomKey();
    }
}
